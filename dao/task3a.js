var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
var _ = require('lodash');

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

fetch(pcUrl)
    .then(function(res) {
        return res.json();
    }).then(function(data) {

        router.get('/volumes', function (req, res) {
            res.json({
                "C:":"41943040B",
                "D:":"16777216B"
            });
        });

        router.get('/:param1?/:param2?/:param3?', function (req, res) {
            var pattern = '';
            var result = [data];
            console.log(req.params);
            _.each(req.params, function (val) {
                if (val) pattern += val + '.';
            });
            pattern = pattern.slice(0, -1);
            if (pattern) {
                result = _.at(data, pattern);
            }
            if (result[0] || result[0] === null || result[0] === 0) {
                res.status(200).json(result[0]);
            } else {
                res.status(404).send("Not Found");
            }
        });
});

module.exports = router;