var Skb = require('skb');
var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ODFjNWM1MDE1OGRjNzAwMTJkNzEzOTkiLCJ1c2VybmFtZSI6ImtyYXZldHM3QG1ldGEudWEiLCJyb2xlIjoidXNlciIsImlhdCI6MTQ3ODI1MzY0OX0.8JUwz2E0PNoIdIp_e68riLAyO1upSWwju0PrJyJHZ8Q';
var skb = new Skb(token);
var express = require('express');
var app = express();
var _ = require('lodash');
var URL = require('url-parse');
var normalizeUrl = require('normalize-url');
//skb.taskHelloWorld('Любой ваш текст тут');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/task2a', function (req, res) {
    var a = +req.query.a || 0;
    var b = +req.query.b || 0;
    res.status(200).send(a+b+'');
});

app.get('/task2b', function (req, res) {
    var fullname = req.query.fullname || '';
    console.log(fullname);
    if (fullname) {
        var data = fullname.split(' ');
        switch (data.length) {
            case 3:
                res.status(200).send(data[2] + ' ' + data[0][0] + '. ' + data[1][0] + '.');
                break;
            case 2:
                res.status(200).send(data[1] + ' ' + data[0][0] + '.');
                break;
            case 1:
                res.status(200).send(data[0]);
                break;
            default:
                res.status(200).send('Invalid fullname');
        }
    } else {
        res.status(200).send('Invalid fullname');
    }
});

app.get('/task2c', function (req, res) {
    var url = new URL((req.query.username.indexOf('/') !== -1) ? normalizeUrl(req.query.username) : req.query.username);
    console.log(url);
    var result = url.pathname.split('/');
    console.log(result);
    var username = (result[0] === '')
        ? result[1]
        : result[0];
    res.status(200).send((username.indexOf('@')!==0) ? '@' + username : username);
});

app.get('/task2d', function (req, res) {
    console.log(req.query.color);
    var color = req.query.color;
    if (!color) {
        badRequest();
    } else {
        color = color.trim();
        if (color[0] === '#') color = color.slice(1);
        console.log(color.length);
        if (color.length !==3 && color.length !==6) return badRequest();
        var result = [];
        if (color.length === 3) color = color[0]+color[0]+color[1]+color[1]+color[2]+color[2];
        _.each(color, function (char) {
            var parsed = parseInt (char, 16);
            result.push(isNaN(parsed));
        });
        console.log(result);
        var status = result.every(function (item) {
            return item === false;
        });
        console.log(status);
        if (!status) {
            return badRequest();
        }
        return res.status(200).send('#' + color.toLowerCase());
    }

    function badRequest() {
        return res.status(400).send('Invalid color');
    }

});

app.use('/task3a', require('./dao/task3a'));

app.listen(8081, function () {
    console.log('server start on 8081');
});

//https://bitbucket.org/yevhen_kravets/skillbranch